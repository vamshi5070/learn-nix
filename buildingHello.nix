{lib,stdenv}:
stdenv.mkDerviation rec {
    pname = "hello";
    version = "2.12";

    src = builtins.fetchTarball {
        url = "mirror://gnu/${pname}/${pname}-${version}.tar.gz";
        sha256 = "000000000000000000000000000000000000000000000000000";
    };
}