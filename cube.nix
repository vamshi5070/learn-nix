# let cube = x: y: z: x * y * z; in (cube 2 4 5)

# nix is lazy by default
# a.b.c = 1;

{
    
   x = 1; 
    
    
    a = {
     b = {
        c = 1;
    };
};

    cmplx = let a = {
        x = 1;
        y = 2;
        z = 3;
    };
    in 
    with a; [x y z];

    func = x: x + 1;
}