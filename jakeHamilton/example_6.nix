let
  pkgs = import <nixpkgs> {};
in
builtins.derivation {
  name = "my-derivation";
  builder = pkgs.bash;
# "/bin/sh";
  system = "aarch64-linux";

  args = [ "-c" "echo Hello > $out"];
}
