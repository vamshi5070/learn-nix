# nix is bash based
{
  inputs = {
    nixpkgs = {
      url = "github:nixos/nixpkgs/nixos-23.05";
    };

    flake-utils = {
      url = "github:numtime/flake-utils";
    }
  };
  outputs = inputs:
    let
      system = "aarch64-linux";
      pkgs = inputs.nixpkgs.legacyPackages.${system};
    in
      rec {
        packages.${system} = {};
        devShells.${system} = rec {
          default = myshell;
          
          myshell = pkgs.mkShell {
            buildInputs = [pkgs.hello];
          };
        };
        };
}
