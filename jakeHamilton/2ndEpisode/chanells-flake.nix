# nix is bash based
{
  inputs = {
    nixpkgs = {
      url = "github:nixos/nixpkgs/nixos-23.05";
    };

    flake-utils-plus = {
      url = "github:gytis-ivaskevicius/flake-utils-plus";
    };
      
  };
  outputs = inputs@{nixpkgs, flake-utils-plus, self}:
    inputs.flake-utils-plus.lib.mkFlake {
      inherit inputs self;
      outputsBuilder = channels: {
        packages = {
          hello = channels.nixpkgs.hello;
        };
      };
  };
}
