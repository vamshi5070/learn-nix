# nix is bash based
{
  inputs = {
    nixpkgs = {
      url = "github:nixos/nixpkgs/nixos-23.05";
    };
  };
  outputs = inputs:
    let
      system = "aarch64-linux";
      pkgs = inputs.nixpkgs.legacyPackages.${system};
    in
      {
        packages.${system} = {
          my-derivation  = pkgs.stdenv.mkDerivation {
            name = "my-derivation";
            src = ./.;
            installPhase = ''
                         echo Hello > $out
                         '';
                         # cp ./script.sh $out
            nativeBuildInputs = [
              pkgs.hello
            ];
          };
          my-hello = pkgs.hello;
          };
        };
}
