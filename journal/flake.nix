{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    
  };
  outputs = {self,nixpkgs}: {
    defaultPackage.aarch64-linux =
      with import nixpkgs {system = "aarch64-linux";};
      stdenv.mkDerivation {
        name = "my_journal";
        src = self;
        # buildPhase = "ls";
        buildInputs = [pkgs.gcc];
        buildPhase = "gcc -o my_journal ./src/journal.c";
        installPhase = "mkdir -p $out/bin; install -t $out/bin my_journal";
      };
  };
}
