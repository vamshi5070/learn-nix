#include <stdio.h>
#include <time.h>

int main(int argc, char *argv[]){
  FILE *file;
  time_t currentTime;
  struct tm *timeInfo;
  char timeString[50];

  // Get current time
  currentTime = time(NULL);

  // Convert it to localtime
  timeInfo = localtime(&currentTime);

  // Formatting time to string
  strftime(timeString,sizeof(timeString),"%H:%M:%S", timeInfo);
  
  file = fopen("defaultJournal.txt", "a");

  if (file == NULL){
    printf("Unable to open the file.\n");
    return 1;
  }

  if (argc == 2){
    printf("%s","this is what i want \n");
    printf("%s",timeString);
    char *temp = argv[1];
    printf("%s",temp);
    
    fprintf(file,"%s -",timeString);
    fprintf(file,"%s ",temp);
    /* fprintf(file,"Hello world!\n"); */
    /* fprintf(file,"This is writing a text!!!!!!!!\n"); */
    
    fclose(file);

  } else {
    printf("%s","more than 1\n");
  }

  return 0;
}
