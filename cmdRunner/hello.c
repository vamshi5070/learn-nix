#include <stdio.h>

int main(){
  FILE *file;

  file = fopen("defaultJournal.txt", "w");

  if (file == NULL){
    printf("Unable to open the file.\n");
    return 1;
  }

  fprintf(file,"Hello world!\n");
  fprintf(file,"This is writing a text!!!!!!!!\n");

  fclose(file);
  return 0;
}
