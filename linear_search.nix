let 
    linearSearch = arr: target:
        let 
            searchStep = i:
                if i < builtins.length arr then
                    if builtins.elemAt arr  i == target then
                        i
                    else
                        searchStep (i + 1) 
                else
                    -1;
        in
        searchStep 0;
in
{
x = linearSearch [3 5 8 109] 100;
}