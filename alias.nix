let
  pkgs  = import <nixpkgs>{};
in
pkgs.stdenv.mkDerivation {
  name = "hello";
  src = ./.;
  buildInputs = [pkgs.gcc];
  buildPhase = "gcc -o hello ./hello.c";
  installPhase = ''
               mkdir -p  $out/bin; install -t $out/bin hello
               '';
}
