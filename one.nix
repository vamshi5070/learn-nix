let pkg =  {
      x = 1;
      y = "This is y";
    };
in
{ x = 1;
  res =  map (x: x * 2) [ 2 3 4 5 6];
  # inherit pkg;
  kmonad = pkg.y;
}.kmonad

